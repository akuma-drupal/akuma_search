<?php

/**
 * @file
 * Replace Default Control Sopported Index to dont support "akuma_search" //SearchApiAlterLanguageControl.
 */

/**
 * Search API data alteration callback that filters out items based on their
 * bundle.
 */
class SearchApiReplaceLanguageControl extends SearchApiAlterLanguageControl {
  /**
   * Overrides SearchApiAbstractAlterCallback::supportsIndex().
   *
   * Only returns TRUE if the system is multilingual and not "Akuma Search".
   *
   * @see drupal_multilingual()
   */
  public function supportsIndex(SearchApiIndex $index) {
    return parent::supportsIndex($index) && ($index->item_type !== 'akuma_search');
  }
}
