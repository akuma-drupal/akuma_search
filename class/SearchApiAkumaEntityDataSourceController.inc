<?php

/**
 * User  : Nikita
 * Date  : 11/2/15
 * E-Mail: mesaverde228@gmail.com
 */
class SearchApiAkumaEntityDataSourceController extends SearchApiAbstractDataSourceController {

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, array &$form_state) {
    $form['_bundles'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Entity Bundles'),
      '#description' => t('Select the entity bundles which should be included in this index.'),
      '#tree'        => TRUE
    );
    $supported = array(
      'node',
      'taxonomy_term',
    );
    $options = search_api_entity_bundle_options_list();
    foreach ($supported as $entity_type) {
      if (isset($options[$entity_type])) {
        $info = entity_get_info($entity_type);
        $form['_bundles'][$entity_type] = array(
          '#type'     => 'checkboxes',
          '#title'    => $info['label'],
          '#options'  => $options[$entity_type],
          '#disabled' => !empty($form_state['index']),
        );
      }
    }

    if (!empty($form_state['index']->options['datasource']['_bundles'])) {
      foreach ($form_state['index']->options['datasource']['_bundles'] as $entity_id => $selected) {
        $form['_bundles'][$entity_id]['#default_value'] = $selected;
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationFormValidate(array $form, array &$values, array &$form_state) {
    $data = array();
    foreach ($values['_bundles'] as $entity_type => $bundles) {
      foreach ($bundles as $k => $v) {
        if ($v === 0) {
          continue;
        }
        $data[$entity_type][$k] = $v;
      }
    }
    if (empty($data)) {
      form_set_error('_bundles', 'Select at least one entity bundle');
    }
    else {
      $values['_bundles'] = $data;
    }
  }

  /**
   * Retrieves the index for which the current method was called.
   *
   * Very ugly method which uses the stack trace to find the right object.
   *
   * @return SearchApiIndex
   *   The active index.
   *
   * @throws SearchApiException
   *   Thrown if the active index could not be determined.
   */
  protected function getCallingIndex() {
    foreach (debug_backtrace() as $trace) {
      if (isset($trace['object'])) {
        if ($trace['object'] instanceof SearchApiIndex) {
          return $trace['object'];
        }
      }

      if (isset($trace['args']) && count($trace['args'])) {
        foreach ($trace['args'] as $arg) {
          if (is_object($arg) && ($arg instanceof SearchApiQueryInterface)) {
            /** Try To Get Current Index From Query Object */
            $index = $arg->getIndex();
            if ($index instanceof SearchApiIndex) {
              $class = __CLASS__;
              if ($index->datasource() instanceof $class) {
                return $index;
              }
            }
          }
        }
      }
    }
    // If there's only a single index on the site, it's also easy.
    $indexes = search_api_index_load_multiple(FALSE);
    if (count($indexes) === 1) {
      return reset($indexes);
    }
    throw new SearchApiException('Could not determine the active index of the datasource.');
  }

  /**
   * Returns the entity types for which this datasource is configured.
   *
   * Depends on the index from which this method is (indirectly) called.
   *
   * @param SearchApiIndex $index
   *   (optional) The index for which to get the enabled entity types. If not
   *   given, will be determined automatically.
   *
   * @return string[]
   *   The machine names of the datasource's enabled entity types, as both keys
   *   and values.
   *
   * @throws SearchApiException
   *   Thrown if the active index could not be determined.
   */
  protected function getAkumaEntityBundles(SearchApiIndex $index = NULL) {
    if (!$index) {
      $index = $this->getCallingIndex();
    }
    if (isset($index->options['datasource']['_bundles'])) {
      return $index->options['datasource']['_bundles'];
    }
    return array();
  }

  /**
   * Returns the selected entity type options for this datasource.
   *
   * Depends on the index from which this method is (indirectly) called.
   *
   * @param SearchApiIndex $index
   *   (optional) The index for which to get the enabled entity types. If not
   *   given, will be determined automatically.
   *
   * @return string[]
   *   An associative array, mapping the machine names of the enabled entity
   *   types to their labels.
   *
   * @throws SearchApiException
   *   Thrown if the active index could not be determined.
   */
  protected function getSelectedAkumaEntityBundlesOptions(SearchApiIndex $index = NULL) {
    $all_bundles = search_api_entity_bundle_options_list();
    $selected = $this->getAkumaEntityBundles($index);
    foreach ($selected as $type => $data) {
      $selected[$type] = array_intersect_key($all_bundles[$type], $data);
    }
    return $selected;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurationSummary(SearchApiIndex $index) {
    $return = NULL;
    $type_labels = $this->getSelectedAkumaEntityBundlesOptions($index);
    if ($type_labels) {
      foreach ($type_labels as $entity_type => $bundles) {
        $info = entity_get_info($entity_type);
        $args['!type'] = $info['label'];
        $args['!bundles'] = implode(', ', $bundles);
        $return .= format_plural(count($type_labels), 'Indexed entity bundles for "!type" : !bundles.', 'Indexed entity bundles for "!type" : !bundles.', $args);
      }
    }
    return $return;
  }

  /** FIELDS SECTION */

  /**
   * {@inheritdoc}
   */
  protected function getPropertyInfo() {
    $info = array(
      'item_id'        => array(
        'label'       => t('ID'),
        'description' => t('The combined ID of the item, containing both entity type and entity ID.'),
        'type'        => 'token',
      ),
      'item_type'      => array(
        'label'        => t('Entity type'),
        'description'  => t('The entity type of the item.'),
        'type'         => 'token',
        'options list' => 'search_api_entity_type_options_list',
      ),
      'item_entity_id' => array(
        'label'       => t('Entity ID'),
        'description' => t('The entity ID of the item.'),
        'type'        => 'token',
      ),
      'item_bundle'    => array(
        'label'        => t('Bundle'),
        'description'  => t('The bundle of the item, if applicable.'),
        'type'         => 'token',
        'options list' => 'search_api_combined_bundle_options_list',
      ),
      'item_label'     => array(
        'label'           => t('Label'),
        'description'     => t('The label of the item.'),
        'type'            => 'text',
        // Since this needs a bit more computation than the others, we don't
        // include it always when loading the item but use a getter callback.
        'getter callback' => 'akuma_search_akuma_type_item_label',
      ),
    );

    foreach ($this->getSelectedAkumaEntityBundlesOptions() as $type => $data) {
      $label = entity_get_info($type)['label'];
      $info[$type] = array(
        'label'       => $type,
        'description' => t('The indexed entity, if it is of type %type.', array('%type' => $label)),
        'type'        => $type,
      );
    }

    return array('property info' => $info);
  }

  /** STATUS */
  /**
   * {@inheritdoc}
   */
  protected $table = 'search_akuma_item';
  protected $entity_table = 'search_akuma_item_entity';

  /**
   * {@inheritdoc}
   */
  public function getIndexStatus(SearchApiIndex $index) {
    if (!$this->table) {
      return array('indexed' => 0, 'total' => 0);
    }
    $this->checkIndex($index);

    $indexed = db_select($this->table, 'i')
      ->condition($this->indexIdColumn, $index->id)
      ->condition($this->changedColumn, 0)
      ->countQuery()
      ->execute()
      ->fetchField();

    $total = db_select($this->table, 'i')
      ->condition($this->indexIdColumn, $index->id)
      ->countQuery()
      ->execute()
      ->fetchField();
    return array('indexed' => $indexed, 'total' => $total);
  }

  /** INTERNAL USAGE */

  /**
   * {@inheritdoc}
   */
  public function getIdFieldInfo() {
    return array(
      'key'  => 'item_id',
      'type' => 'string',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getItemId($item) {
    return isset($item->item_id) ? $item->item_id : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemLabel($item) {
      return akuma_search_akuma_type_item_label($item);
  }

  /**
   * {@inheritdoc}
   */
  public function getItemUrl($item) {
    if ($item->item_type == 'file') {
      return array(
        'path'    => file_create_url($item->file->uri),
        'options' => array(
          'entity_type' => 'file',
          'entity'      => $item,
        ),
      );
    }

    $url = entity_uri($item->item_type, $item->{$item->item_type});
    return $url ? $url : NULL;
  }

  protected function getEntityByInternalIds($ids) {
    return db_select($this->entity_table, 'et')->condition('id', (array) $ids, 'IN');
  }

  /**
   * {@inheritdoc}
   */
  public function loadItems(array $ids) {
    if(count($ids)<=0){
      return array();
    }
    $int_items = db_select($this->entity_table, 'et')->fields('et')->condition('id', $ids, 'IN')->execute()
      ->fetchAllAssoc('id');

    $items = array();
    foreach ($int_items as $id => $int_item) {
      $type = $int_item->entity_type;
      $entity_id = $int_item->entity_id;
      $entity = entity_load_single($type, $entity_id);

      $item = (object) array($type => $entity);
      $item->item_id = $id;
      $item->item_type = $type;
      $item->{$item->item_type} = $entity;
      $item->entity = & $item->{$item->item_type};
      $item->item_entity_id = $entity_id;
      $item->item_bundle = NULL;
      if (isset($entity->language)) {
        $item->language = $entity->language;
      }
      $items[$id] = $item;
    }

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function startTracking(array $indexes) {
    if (!$this->table) {
      return;
    }
    // We first clear the tracking table for all indexes, so we can just insert
    // all items again without any key conflicts.
    $this->stopTracking($indexes);
    /** REFRESH */
    akuma_search_item_entity_update();

    foreach ($indexes as $index) {
      $types = $this->getAkumaEntityBundles($index);

      // Wherever possible, use a sub-select instead of the much slower
      // entity_load().
      foreach ($types as $type => $bundles) {
        $entity_info = entity_get_info($type);

        if (!empty($entity_info['base table'])) {
          /** We try To get All items from {search_akuma_item_entity}  */
          if (isset($entity_info['base table'])) {
            $id_field = $entity_info['entity keys']['id'];
            $bundle_field = $entity_info['entity keys']['bundle'];
            $table = $entity_info['base table'];

            $query = db_select($this->entity_table, 'et');
            $query->condition('entity_type', $type);
            $query->addField('et', 'id', 'item_id');
            $query->addExpression(':index_id', 'index_id', array(':index_id' => $index->id));
            $query->addExpression('1', 'changed');
            $query->join($table, 'bt', 'et.entity_id = bt.' . $id_field);

            switch ($bundle_field) {
              case 'vocabulary_machine_name':
                $query->join('taxonomy_vocabulary', 'tv', 'bt.vid = tv.vid');
                $query->condition('tv.machine_name', $bundles);
                break;
              default:
                $query->condition($bundle_field, $bundles);
                break;
            }

            // INSERT ... SELECT ... ENTITIES
            db_insert($this->table)
              ->from($query)
              ->execute();
            unset($types[$type]);
          }
        }
      }
      // In the absence of a "base table", use the slow entity_load().
//      if ($types) {
//        foreach ($types as $type) {
//          $query = new EntityFieldQuery();
//          $query->entityCondition('entity_type', $type);
//          $result = $query->execute();
//          $ids = !empty($result[$type]) ? array_keys($result[$type]) : array();
//          if ($ids) {
//            foreach ($ids as $i => $id) {
//              $ids[$i] = $type . '/' . $id;
//            }
//            $this->trackItemInsert($ids, array($index), TRUE);
//          }
//        }
//      }
    }
  }


  /**
   * Starts tracking the index status for the given items on the given indexes.
   *
   * @param array            $item_ids
   *   The IDs of new items to track.
   * @param SearchApiIndex[] $indexes
   *   The indexes for which items should be tracked.
   * @param bool             $skip_type_check
   *   (optional) If TRUE, don't check whether the type matches the index's
   *   datasource configuration. Internal use only.
   *
   * @return SearchApiIndex[]|null
   *   All indexes for which any items were added; or NULL if items were added
   *   for all of them.
   *
   * @throws SearchApiDataSourceException
   *   If any error state was encountered.
   */
  public function trackItemInsert(array $item_ids, array $indexes, $skip_type_check = FALSE) {
    $ret = array();
    foreach ($indexes as $index_id => $index) {
      $ids = drupal_map_assoc($item_ids);
      if (!$skip_type_check) {
        $types = $this->getAkumaEntityBundles($index);

        $int_items = db_select($this->entity_table, 'et')->fields('et')->condition('id', $ids, 'IN')->execute()
          ->fetchAllAssoc('id');
        foreach ($int_items as $int_id => $int_item) {
          $entity = entity_load_single($int_item->entity_type, $int_item->entity_id);
          $wrapper = entity_metadata_wrapper($int_item->entity_type, $entity);

          if (!in_array($wrapper->getBundle(), $types[$int_item->entity_type])) {
            unset($ids[$int_id]);
          };

        }
      }

      if ($ids) {
        parent::trackItemInsert($ids, array($index));
        $ret[$index_id] = $index;
      }
    }
    return $ret;
  }


  /**
   * {@inheritdoc}
   */
  public function getMetadataWrapper($item = NULL, array $info = array()) {
    /** AutoComplete Elastic Search Fix */
    if (empty($info)) {
      $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
      $info[] = $this;

      if (isset($trace[1]) && (isset($trace[1]['function']) && ($trace[1]['function'] == 'getAutocompleteSuggestions'))) {

        return entity_metadata_wrapper('node', NULL, $info);
      }
    }

    return parent::getMetadataWrapper($item, $info);
  }
}