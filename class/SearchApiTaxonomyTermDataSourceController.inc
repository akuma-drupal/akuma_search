<?php
/**
 * User  : Nikita
 * Date  : 11/2/15
 * E-Mail: mesaverde228@gmail.com
 */

class SearchApiTaxonomyTermDataSourceController extends SearchApiEntityDataSourceController{
  /**
   * {@inheritdoc}
   */
  public function startTracking(array $indexes) {
    if (!$this->table) {
      return;
    }
    // We first clear the tracking table for all indexes, so we can just insert
    // all items again without any key conflicts.
    $this->stopTracking($indexes);

    if (!empty($this->entityInfo['base table']) && $this->idKey) {
      // Use a subselect, which will probably be much faster than entity_load().

      // Assumes that all entities use the "base table" property and the
      // "entity keys[id]" in the same way as the default controller.
      $table = $this->entityInfo['base table'];

//      var_dump($this->entityInfo);die();

      // We could also use a single insert (with a UNION in the nested query),
      // but this method will be mostly called with a single index, anyways.
      foreach ($indexes as $index) {
        // Select all entity ids.
        $query = db_select($table, 't');
        $query->addField('t', $this->idKey, 'item_id');
        $query->addExpression(':index_id', 'index_id', array(':index_id' => $index->id));
        $query->addExpression('1', 'changed');
        $query->join('taxonomy_vocabulary', 'tv', 't.vid=tv.vid');
        if ($bundles = $this->getIndexBundles($index)) {
          $query->condition('tv.machine_name', $bundles);
        }

//        var_dump($this->table, $query->execute());die();
        // INSERT ... SELECT ...
        db_insert($this->table)
          ->from($query)
          ->execute();
      }
    }
    else {
      // In the absence of a 'base table', use the slower way of retrieving the
      // items and inserting them "manually". For each index we get the item IDs
      // (since selected bundles might differ) and insert all of them as new.
      foreach ($indexes as $index) {
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', $this->entityType);
        if ($bundles = $this->getIndexBundles($index)) {
          $query->entityCondition('bundle', $bundles);
        }
        $result = $query->execute();
        $ids = !empty($result[$this->entityType]) ? array_keys($result[$this->entityType]) : array();
        if ($ids) {
          $this->trackItemInsert($ids, array($index));
        }
      }
    }
  }
} 