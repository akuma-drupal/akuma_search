<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Contain handlers for hooks schema
 */

/**
 *
 * Implements hook_schema().
 *
 */
function akuma_search_schema() {
  $schema = array();
  /**
   * Akuma Search Index Ids Similar to "search_api_string_id" table but with own autoincrement
   */
  $schema['search_akuma_item_entity'] = array(
    'description' => 'Stores all entities for indexes on a {search_akuma_item}.',
    'fields'      => array(
      'id'          => array(
        'description' => 'An integer identifying the index\' item.',
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
      'entity_type' => array(
        'description' => 'An entity type (machine_name).',
        'type'        => 'varchar',
        'length'      => 64,
        'not null'    => TRUE,
      ),
      'entity_id'   => array(
        'description' => 'An entity integer identifier.',
        'type'        => 'int',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
    ),
    'indexes'     => array(
      'item_type' => array('entity_type'),
    ),
    'unique keys' => array(
      'type_id_index' => array('entity_type', 'entity_id'),
    ),
    'primary key' => array('id'),
  );

  /**
   * Akuma Search Entity Index, FK
   */
  $schema['search_akuma_item'] = array(
    'description' => 'Stores the items which should be indexed for each index, and their status.',
    'fields'      => array(
      'item_id'  => array(
        'description' => "The item's id (e.g. {search_akuma_item_entity}.id).",
        'type'        => 'int',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
      'index_id' => array(
        'description' => 'The {search_api_index}.id this item belongs to.',
        'type'        => 'int',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
      'changed'  => array(
        'description' => 'Either a flag or a timestamp to indicate if or when the item was changed since it was last indexed.',
        'type'        => 'int',
        'size'        => 'big',
        'not null'    => TRUE,
        'default'     => 1,
      ),
    ),
    'indexes'     => array(
      'indexing' => array('index_id', 'changed'),
    ),
    'primary key' => array('item_id', 'index_id'),
  );
  return $schema;
}

/**
 *
 * Implements hook_schema_alter().
 *
 */
function akuma_search_schema_alter(&$schema) {
}