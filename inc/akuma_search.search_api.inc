<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Contain handlers for hooks search_api
 */
include_once DRUPAL_ROOT . '/includes/utility.inc';
if (!function_exists('search_api_entity_bundle_options_list')) {
    function search_api_entity_bundle_options_list($entity_type = null)
    {
        $ret = array();
        $info = entity_get_info($entity_type);
        foreach ($info as $entity_id => $entity_info) {
            foreach ($entity_info['bundles'] as $bundle_id => $bundle_info) {
                $ret[$entity_id][$bundle_id] = isset($bundle_info['label']) ? $bundle_info['label'] : $bundle_id;
            }
        }
        if (!empty($entity_type)) {
            if (isset($ret[$entity_info])) {
                return array($entity_type => $ret[$entity_info]);
            } else {
                return null;
            }
        }

        return $ret;
    }
}

/**
 * Implements hook_search_api_item_type_info().
 */
function akuma_search_search_api_item_type_info()
{
    $types = array();

    $types['akuma_search'] = array(
      'name'                  => 'Akuma Entity',
      'datasource controller' => 'SearchApiAkumaEntityDataSourceController',
      'module'                => 'akuma_search',
    );

    return $types;
}

function akuma_search_search_api_item_type_info_alter(&$info)
{
    $info['taxonomy_term'] = array(
      'name'                  => 'Taxonomy term',
      'datasource controller' => 'SearchApiTaxonomyTermDataSourceController',
      'entity_type'           => 'taxonomy_term',
      'module'                => 'search_api',
    );
    $info['taxonomy_vocabulary'] = array(
      'name'                  => 'Taxonomy vocabulary',
      'datasource controller' => 'SearchApiTaxonomyVocabularyDataSourceController',
      'entity_type'           => 'taxonomy_vocabulary',
      'module'                => 'search_api',
    );

    return $info;
}

/**
 * Implements hook_search_api_alter_callback_info().
 */
function akuma_search_search_api_alter_callback_info()
{
    $callbacks = array();
//  $callbacks['search_api_alter_add_url'] = array(
//    'name'        => t('URL field'),
//    'description' => t("Adds the item's URL to the indexed data."),
//    'class'       => 'AkumaSearchApiAlterAddUrl',
//  );
//  $callbacks['search_api_alter_language_control'] = array(
//    'name'        => t('Language control'),
//    'description' => t('Lets you determine the language of items in the index.'),
//    'class'       => 'SearchApiReplaceLanguageControl',
//  );

    return $callbacks;
}

function akuma_search_search_api_alter_callback_info_alter(&$callbacks)
{
    if (isset($callbacks['search_api_alter_language_control'])) {
        $callbacks['search_api_alter_language_control']['class'] = 'SearchApiReplaceLanguageControl';
    }
}


/**
 * Lets modules alter an Elastic search request before sending it.
 *
 * @param SearchApiQueryInterface $query
 *   The SearchApiQueryInterface object representing the executed search query.
 * @param array                   $params
 *   An associative array containing the index being queried, the type,
 *   the fields, the query_string etc.
 */
function akuma_search_elasticsearch_connector_search_api_query_alter($query, &$params)
{
//    if ('search_api_elasticsearch_connector' == $query->getIndex()->server()->class) {
//        file_put_contents('c:/' . __FUNCTION__ . 'query.txt', print_r($query, true));
//        $params['body']['query']['filtered']['query']['bool']['must']['0']['multi_match']['query'] = '*' . trim($params['body']['query']['filtered']['query']['bool']['must']['0']['multi_match']['query'],'*') .'*';
//        file_put_contents('c:/' . __FUNCTION__ . 'parems.txt', print_r($params, true));
//        var_dump($query->getIndex()->server());
//        die();
//    }
}

/**
 * Lets modules alter the search results returned from an Elastic search.
 *
 * @param array                   $results
 *   The results array that will be returned for the search.
 * @param SearchApiQueryInterface $query
 *   The SearchApiQueryInterface object representing the executed search query.
 * @param array                   $response
 *   The Elastic Search response array.
 */
function akuma_search_elasticsearch_connector_search_api_results_alter(&$results, $query, $response)
{
    foreach ($results['results'] as &$result) {
        $entity = entity_load_single($result['fields']['item_type'], $result['fields']['item_entity_id']);
        $lang = $result['fields']['search_api_language'];
        $uri = entity_uri($result['fields']['item_type'], $entity);
        $url = url($uri['path'], array('absolute' => true, 'language' => $lang));
        /** Hardocdelly APPEND Link */
        $result['fields']['item_label'] = '<a href="' . $url . '">' . $result['fields']['item_label'] . '</a>';
        if (isset($result['highlight']['item_label'][0])) {
            $result['highlight']['item_label'][0] = '<a href="' . $url . '">' . $result['highlight']['item_label'][0] . '</a>';
        }
    }
}


/**
 * Implement hook_form_alter for the exposed form.
 *
 * Since the exposed form is a GET form, we don't want it to send a wide
 * variety of information.
 */
function akuma_search_form_views_exposed_form_alter(&$form, &$form_state)
{
    array_unshift($form['#validate'], 'akuma_search_exposed_form_validate');
    array_unshift($form['#submit'], 'akuma_search_exposed_form_submit');

}

/**
 * Validate handler for exposed filters
 */
function akuma_search_exposed_form_validate(&$form, &$form_state)
{
    if ($form_state['view']->query instanceof SearchApiViewsQuery) {
        /** @var SearchApiViewsQuery $query */
        $query = $form_state['view']->query;
        if ('search_api_elasticsearch_connector' == $query->getIndex()->server()->class) {
            foreach ($form['#info'] as $filter_name => $info) {
                if ('search_api_views_fulltext_op' == $info['operator']) {
                    $value_name = $info['value'];
                    if (drupal_strlen(trim($form[$value_name]['#value'], '*'))) {
                        $value = '*' . trim($form[$value_name]['#value'], '*') . '*';
//                        $form_state['view']->exposed_input[$value_name] = $value;
//                        $form_state['display']->handler->view->exposed_input[$value_name] = $value;
//                        $form_state['input'][$value_name] = $form[$value_name]['#value'];
                        $form_state['values'][$value_name] = $value;
                    }
                }
            }
        }
    }

}

/**
 * Submit handler for exposed filters
 */
function akuma_search_exposed_form_submit(&$form, &$form_state)
{

}
