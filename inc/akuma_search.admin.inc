<?php
/**
 * User  : Nikita.Makarov
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 */


/**
 * Form builder; Configure settings.
 *
 * @see     system_settings_form()
 */
function akuma_search_admin_settings_form($form, $form_state)
{
    $form = array();

    $form['#submit'][] = 'akuma_search_admin_settings_form_submit';

    return system_settings_form($form);
}

/**
 * @param $form
 * @param $form_state
 */
function akuma_search_admin_settings_form_submit($form, &$form_state)
{

}
function akuma_search_search_api_autocomplete_page_replacement(SearchApiAutocompleteSearch $search, $fields, $keys = '') {
//            var_dump(function_exists('elasticsearch_connector_search_api_search_api_autocomplete_suggestions_alter'),__FILE__,__LINE__);die();
    $service_info = search_api_get_service_info($search->index()->server()->wrapper()->class->value());
    $service_class = $service_info['class'];
    if(!function_exists('elasticsearch_connector_search_api_search_api_autocomplete_suggestions_alter') || (in_array('SearchApiElasticsearchConnector',class_implements($service_class)))){
    }else{
        $modules = module_implements('search_api_autocomplete_suggestions_alter');
        $imps = &drupal_static('module_implements');
        /** Remove Hacked Alteration From ElasticSearch */
        unset($imps['search_api_autocomplete_suggestions_alter']['elasticsearch_connector_search_api']);
    }
    search_api_autocomplete_autocomplete($search,$fields,$keys);

}
function akuma_search_admin_overview($original_callback = 'search_api_admin_overview')
{
    $table = $original_callback();
    $servers = search_api_server_load_multiple(false);
    $indexes = array();
    foreach (search_api_index_load_multiple(false) as $index) {
        $indexes[$index->machine_name] = $index;
    }
    $service_info = search_api_get_service_info();
    $table['#header'][4] = $table['#header'][2];
    $table['#header'][5] = $table['#header'][3];
    $table['#header'][2] = 'Title';
    $table['#header'][3] = 'Class';

    foreach ($table['#rows'] as $k => &$row) {
        if (count($row) == 4) {
            //Server
            preg_match('/^<a.*?href=(["\'])(.*?)\1.*$/', $row[2], $m);
            $machine_name = explode('admin/config/search/search_api/server/', $m[2]);
            $machine_name = $machine_name[1];
            array_keys($servers);
            $row[4] = $row[2];
            $row[5] = $row[3];
            $row[3] = 'UNKNOWN';
            $row[2] = 'UNKNOWN';
            if (isset($servers[$machine_name])) {
                if (isset($service_info[$servers[$machine_name]->class])) {
                    $row[2] = '<span style="font-style:italic;" >' . $service_info[$servers[$machine_name]->class]['name'] . '</span>';
                    $row[3] = '<span style="font-style:italic;" >' . $service_info[$servers[$machine_name]->class]['class'] . '</span>';
                } else {
                    $row[2] = '<span style="font-style:italic;" title="">' . $servers[$machine_name]->class . '</span>';
                    $row[3] = '<span style="font-style:italic;" title="">' . $servers[$machine_name]->class . '</span>';
                }
            }
        } elseif (count($row) == 5) {
            //Index
            preg_match('/^<a.*?href=(["\'])(.*?)\1.*$/', $row[3], $m);
            $machine_name = explode('admin/config/search/search_api/index/', $m[2]);
            $machine_name = $machine_name[1];
            $row[5] = $row[3];
            $row[6] = $row[4];

            $row[3] = 'UNKNOWN';
            $row[4] = 'UNKNOWN';

            if (isset($indexes[$machine_name])) {
                $wrapper = entity_metadata_wrapper('search_api_index', $indexes[$machine_name]);
                $type_info = search_api_get_item_type_info($wrapper->item_type->value());
                if ($type_info) {
                    $row[3] = '<span style="font-style:italic;">' . $type_info['name'] . '</span>';
                    $row[4] = '<span style="font-style:italic;">' . $type_info['datasource controller'] . '</span>';
                }else{
                    $row[3] = '<span style="font-style:italic;">' . $wrapper->item_type->value() . '</span>';
                    $row[4] = '<span style="font-style:italic;">' . $wrapper->item_type->value() . '</span>';

                }
            }
        }
    }
    return $table;
}