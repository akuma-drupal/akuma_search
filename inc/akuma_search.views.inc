<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Contain handlers for hooks views
 */
include_once DRUPAL_ROOT . '/includes/utility.inc';
/**
 * Implements hook_views_api().
 */
function akuma_search_views_api() {
  return array(
    'api'  => 3.0,
    'path' => drupal_get_path('module', 'akuma_search') . '/views',
  );
}

/**
 * Implements hook_views_default_views().
 */
function akuma_search_views_default_views() {
  $path = './' . drupal_get_path('module', 'akuma_search') . '/views/default_views/*.inc';
  $views = array();
  foreach (glob($path) as $views_filename) {
    require_once($views_filename);
  }
  return $views;
}

function akuma_search_views_data() {
  $data = array();
  return $data;
}

function akuma_search_views_data_alter(&$data) {

}
