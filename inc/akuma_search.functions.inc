<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Contain handlers for hooks entity_api
 */
function akuma_search_akuma_type_item_label($item) {
    $label = entity_label($item->item_type, $item->{$item->item_type});
    return ($label ? $label : NULL);
}
