<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Contain handlers for hooks entity_api
 */

/**
 * Implements hook_entity_insert().
 *
 * This is implemented on behalf of the SearchApiEntityDataSourceController
 * datasource controller and calls search_api_track_item_insert() for the
 * inserted items.
 *
 * @see search_api_search_api_item_type_info()
 */
function akuma_search_entity_insert($entity, $type) {
  // When inserting a new search index, the new index was already inserted into
  // the tracking table. This would lead to a duplicate-key issue, if we would
  // continue.
  // We also only react on entity operations for types with property
  // information, as we don't provide search integration for the others.
  if ($type == 'search_api_index' || !entity_get_property_info($type)) {
    return;
  }
  list($id) = entity_extract_ids($type, $entity);
  if (isset($id)) {
    if ($item_id = akuma_search_generate_search_item_entity_id($type, $id)) {
      search_api_track_item_insert('akuma_search', array($item_id));
    }
  }
}

function akuma_search_generate_search_item_entity_id($entity_type, $entity_id) {
  if (($entity_type == 'node') || ($entity_type == 'taxonomy_term')) {
    $query = db_select('search_akuma_item_entity', 't');
    $query->condition('entity_type', $entity_type);
    $query->condition('entity_id', $entity_id);
    $query->addField('t', 'id');
    $id = $query->execute()->fetchField();
    if ($id > 0) {
      return $id;
    }
    return db_insert('search_akuma_item_entity')->fields(array(
      'entity_type' => $entity_type,
      'entity_id'   => $entity_id,
    ))->execute();
  }
}

/**
 * Implements hook_entity_update().
 *
 * This is implemented on behalf of the SearchApiEntityDataSourceController
 * datasource controller and calls search_api_track_item_change() for the
 * updated items.
 *
 * It also checks whether the entity's bundle changed and acts accordingly.
 *
 * @see search_api_search_api_item_type_info()
 */
function akuma_search_entity_update($entity, $type) {
  // We only react on entity operations for types with property information, as
  // we don't provide search integration for the others.
  if (!entity_get_property_info($type)) {
    return;
  }
  list($id, , $new_bundle) = entity_extract_ids($type, $entity);
  if ($item_id = akuma_search_generate_search_item_entity_id($type, $id)) {
    // Check if the entity's bundle changed.
    if (isset($entity->original)) {
      list(, , $old_bundle) = entity_extract_ids($type, $entity->original);
      if ($new_bundle != $old_bundle) {
        /** Remove Item From Index */
        search_api_track_item_delete('akuma_search', array($item_id));
        search_api_track_item_insert('akuma_search', array($item_id));
      }
    }
    search_api_track_item_change('akuma_search', array($item_id));
  }
}

/**
 * Implements hook_entity_delete().
 *
 * This is implemented on behalf of the SearchApiEntityDataSourceController
 * datasource controller and calls search_api_track_item_delete() for the
 * deleted items.
 *
 * @see search_api_search_api_item_type_info()
 */
function akuma_search_entity_delete($entity, $type) {
  // We only react on entity operations for types with property information, as
  // we don't provide search integration for the others.
  if (!entity_get_property_info($type)) {
    return;
  }
  list($id) = entity_extract_ids($type, $entity);
  if (isset($id)) {
    $item_id = akuma_search_generate_search_item_entity_id($type, $id);
    search_api_track_item_delete('akuma_search', array($item_id));
    db_delete('search_akuma_item_entity')->condition('entity_type', $type)->condition('entity_id', $id)->execute();
  }
}

