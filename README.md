# Search+ #

This module provides functionality to replace default "multi_index" from "search_api".
Provides full support for ElasticSearch service. Right now you able to index Nodes
and Taxonomy terms inside one index with fulltext search functionality.
As we know ElasticSearch supports only integer based identifiers and this module
provide this requirement when indexing both nodes and taxonomy terms at one index.

### Dependencies ###

* Search (Core module)
* [Search API](https://www.drupal.org/project/search_api)
* [Akuma Taxonomy](https://bitbucket.org/inri13666/akuma_taxonomy)
* OPTIONAL: [ElasticSearch](https://www.drupal.org/project/elasticsearch)
* OPTIONAL: [Search Api Autocomplete](https://www.drupal.org/project/search_api_autocomplete)

### How do I get set up? ###

* Install & configure dependencies
* Install this module
* Add & configure new Index with type "Akuma Entity"
* OPTIONAL: Create new view for new index
* OPTIONAL: Enable autocomplete support

### Limitations ###

At this moment this module tested only with ElasticSearch, but may work with
other service types

### Who do I talk to? ###

* Nikita Makarov <mesaverde228@gmail.com>
* [Issue Tracker](https://bitbucket.org/inri13666/akuma_search/issues?status=new&status=open)
* [Wiki](https://bitbucket.org/inri13666/akuma_search/wiki/Home)